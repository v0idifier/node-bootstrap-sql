const betterSqlite = require('better-sqlite3')

const defaultOptions = {
  fileName: 'db.sqlite3',
}

module.exports = function db(migrations, options = {}) {
  options = { ...defaultOptions, ...options }
  const db = betterSqlite(options.fileName)
  db.prepare(
    `
    CREATE TABLE IF NOT EXISTS bootstrap_ran_migrations(
      id SERIAL PRIMARY KEY,
      migration_index INTEGER NOT NULL UNIQUE
    );
  `,
  ).run()

  const indexes = db
    .prepare(`SELECT migration_index FROM bootstrap_ran_migrations;`)
    .all()
    .map(({ migration_index }) => migration_index)

  console.log(`[bootstrap-sql] already run migrations: ${indexes.join(', ')}`)

  for (let i = 0; i < migrations.length; i++) {
    const migration = migrations[i]
    if (indexes.indexOf(i) !== -1) continue
    console.log(`[bootstrap-sql] running migration ${i} '${migration}'`)
    db.prepare(migration).run()
    db.prepare(
      `INSERT INTO bootstrap_ran_migrations (migration_index) VALUES (?)`,
    ).run(i)
  }

  console.log(`[bootstrap-sql] finished bootstrapping db. :sunglasses:`)

  return db
}
