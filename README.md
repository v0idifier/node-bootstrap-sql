# bootstrap-sql
te genera un sqlite con migraciones super sencillo, para usar para bots entre otras cosas. para node.js

## instalar
```bash
npm add git+https://0xacab.org/v0idifier/node-bootstrap-sql.git
```

## usar
```js
const db = require('bootstrap-sql')([
  'CREATE TABLE beep(id SERIAL PRIMARY KEY);',
  'CREATE TABLE boop(di SERIAL PRIMARY KEY);',
])
```

## api
```js
require('bootstrap-sql')(migrations, options)
```

| argumento | que hace |
|-----------|----------|
| migrations | la configuracion de la DB, el array se le va agregando cosas pero NUNCA hay que sacarle algo/moverlo de lugar. siempre agregar cosas al final |
| options | objecto con opciones. opcional. api: |

| option | tipo | default | que hace |
|--------|------|---------|----------|
| fileName | String | `db.sqlite3` | el nombre del archivo de la DB |

## api de la BD
https://github.com/JoshuaWise/better-sqlite3/blob/HEAD/docs/api.md
